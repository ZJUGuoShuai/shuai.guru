---
date: "2019-05-10T00:00:00Z"
tags: NLP
title: 百度 NLP 教程笔记
toc: true
---

来源：*百度 NLP 教程*。

**语言的意义**：语言是知识和思维的载体。

## NLP 基础技术

### 1. 分词 (Word Segmentation)

**目的**：解决“交叉歧义”问题。（例如：“和尚未”这个本文就具有交叉歧义问题。）

![EWOces.md.png](https://s2.ax1x.com/2019/05/11/EWOces.md.png)

### 2. 命名实体识别 (Named Entity Recognition)

**目的**：解决“未登录词问题”。

![EWOqT1.md.png](https://s2.ax1x.com/2019/05/11/EWOqT1.md.png)

### 3. 词性标注 (Part-Of-Speech Tagging)

### 4. 依存句法分析 (Dependency Parsing)

上面两个结合起来可以解决“结构歧义”问题。

![EWXKmj.md.png](https://s2.ax1x.com/2019/05/11/EWXKmj.md.png)

### 5. 词向量与语义相似度 (Word Embedding & Semantic Similarity)

![EWXM0s.png](https://s2.ax1x.com/2019/05/11/EWXM0s.png)

### 6. 文本语义相似度 (Text Semantic Similarity)

![EWX1kq.png](https://s2.ax1x.com/2019/05/11/EWX1kq.png)

