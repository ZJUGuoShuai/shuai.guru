---
date: "2019-04-03T00:00:00Z"
tags:
- Matplotlib
- Python
- 数据可视化
title: Matplotlib 保存大图技巧
---

使用 Matplotlib 的一个常用操作是将图像保存为图片。因为我的笔记本电脑屏幕分辨率只有 `1366x768`，如果直接点击保存按钮，即使预览窗口最大化，保存的图片也差不多只有720P的分辨率。

但 Matplotlib 提供了一个直接保存图片的接口：`matplotlib.pyplot.savefig`，可以自定义指定 DPI。如果将 DPI 指定为默认 DPI 的两倍，输出的图片也会是长宽都是原来的两倍，以此实现高分辨率保存。

```python
import matplotlib.pyplot as plt

# 创建一个 figure。（注：不建议直接使用 plt 来画图和保存。
#   这里出现的一个问题是，如果直接用 plt.savefig() 将生成一副空白图片，用 fig.savefig()
#   则正常，目前不知道问题出在哪里，所以保险起见还是用 fig 画图。）
fig = plt.figure()

# 创建 subplot
subplot = fig.add_subplot()

# 画图
subplot.plot(X, Y)

# 显示
plt.show()

# 保存
fig.savefig('myfig.png', dpi=240)  # 默认 dpi=100
```

此时产生的图片的宽为3278，恰好为我屏幕分辨率1366的2.4倍，因为我设置的240是默认 DPI 100的2.4倍。

（本文完）