---
date: "2019-02-11T00:00:00Z"
tags:
- Python
title: Python 官方文档解读(1)：66 个内置函数
---

Python 解释器 （CPython 3.7）内置有 66 个函数，这些函数在任何时刻都是可用的。此文是为了对这 66 个函数进行简单的梳理，便于以后可能用到它们时能想到。

## 1. abs(*x*)

返回一个数的绝对值。参数`x`可以是`int`、`float`或`complex`。如果是`complex`，则返回这个复数的大小（模）。

## 2. all(*iterable*)

如果`iterable`的所有元素“是”`True`，才返回`True`，否则返回`False`。**若`iterable`为空，也返回`True`。**等价于：

```python
def all(iterable):
    for element in iterable:
        if not element:
            return False
    return True
```

## 3. any(*iterable*)

只要`iterable`有一个元素“是”`True`，就返回`True`。**若`iterable`为空，返回`False`。**等价于：

```python
def any(iterable):
    for element in iterable:
        if element:
            return True
    return False
```

## 4. ascii(*object*)

像`repr()`一样，但将非 ASCII 字符使用`\x`, `\u`或`\U`来转义。

## 5. bin(*x*)

将整数`x`转换为二进制字符串（以“0b”开头）。如果`x`不是一个 Python `int`对象，它需要定义一个`__index__()`方法来返回一个整数。示例：

```python
>>> bin(3)
'0b11'
>>> bin(-10)
'-0b1010'
```

也可以用`format()`和 *f-string* 来完成：

```python
>>> format(14, '#b'), format(14, 'b')
('0b1110', '1110')
>>> f'{14:#b}', f'{14:b}'
('0b1110', '1110')
```

参考 `format()`。

## 6. *class* bool([*x*])

返回`x`的布尔值。如果`x`省略，也返回`False`。`bool`类是`int`类的子类，它仅有两个示例对象`True`和`False`。

## 7. breakpoint(*args*, ***kws*)

3.7 版新增。

## 8. *class* bytearray([*source*[, *encoding*[, *errors*]]])

返回一个字节数组。参考`bytearray`类和`bytes`类。

如果可选的`source`参数：

1. 是一个字符串，则必须提供一个`encoding`参数，`bytearray()`会使用`str.encoding()`来将字符串转换为字节序列。
2. 是一个整数，则字节数组的大小是这个整数，并且以空字节初始化。
3. 是一个符合`buffer`接口的对象，则字节数组以这个对象的一个只读 buffer 来初始化。
4. 是一个 iterable，那它的元素必须是`[0, 256]` 的整数。字节数组以它的元素来初始化。

若没有参数，则返回一个大小为 0 的字节数组。

## 9. callable(*object*)

如果对象是可调用的，则返回`True`，否则返回`Flase`。注意：即使`callable(object)`返回`True`，`object`也有可能无法调用（为什么？）；但如果返回`False`，则一定无法调用。

如果一个对象有`__call__()`方法，则对象是可调用的。

## 10. chr(i)

返回一个字符（串），这个字符的 Unicode 码点为整数`i`。比如`chr(97)`返回字符串`'a'`，`chr(8364)`返回`'€'`。它是`ord()`的逆反。

## 11. @classmethod

装饰器：将一个普通方法转换为类方法。

一个类方法的第一个参数为隐式参数：此类`cls`，而非此对象`self`。示例：

```python
class C:
    @classmethod
    def f(cls, arg1, arg2, ...): ...
```

可以在类上调用（`C.f()`）也可以在对象上调用（`C().f()`）。

Python 类方法不同于 Java 和 C++ 中的静态方法。如果想使用静态方法，参考本文中的`staticmethod()`。

## 12. compile(*source*, *filename*, *mode*, *flags=0*, *dont_inherit=False*, *optimize=-1*)

将 `source` 编译为 code 或 AST 对象。Code 对象可以被 `exec()` 或 `eval()` 执行。`source` 可以是普通字符串、字节字符串或一个 AST 对象。参考官方 `ast` 模块。

示例：

```python
>>> codeInString = 'a = 5\nb=6\nsum=a+b\nprint("sum =",sum)'
>>> codeObejct = compile(codeInString, 'sumstring', 'exec')

>>> exec(codeObejct)
sum = 11
```

## 13. *class* complex([*real*[, *imag*]])

返回一个复数 `real + imag*1j`。或者将一个字符串（第一个参数）或数字转化为复数。

> 注意：`complex('1+2j')` 是正确的，但 `complex('1 + 2j')` 将抛出 `ValueError`.

## 14. delattr(*object*, *name*)

删除一个对象的属性，`delattr(x, 'foobar')` 等价于 `del x.foobar`。参考`setattr()`

## 15. *class* dict(***kwarg*), dict(*mapping*, *\*\*kwarg*), dict(*iterable*, *\*\*kwarg*)

创建一个字典。

## 16. dir([*object*])

没有参数时，返回当前 local 的符号（标识符）。有参数时，返回`object`的有效属性。

如果`object`有`__dir__()`方法，则会调用它。示例：

```python
>>> import struct
>>> dir()   # show the names in the module namespace  # doctest: +SKIP
['__builtins__', '__name__', 'struct']
>>> dir(struct)   # show the names in the struct module # doctest: +SKIP
['Struct', '__all__', '__builtins__', '__cached__', '__doc__', '__file__',
 '__initializing__', '__loader__', '__name__', '__package__',
 '_clearcache', 'calcsize', 'error', 'pack', 'pack_into',
 'unpack', 'unpack_from']
>>> class Shape:
...     def __dir__(self):
...         return ['area', 'perimeter', 'location']
>>> s = Shape()
>>> dir(s)
['area', 'location', 'perimeter']
```

> 注意：`dir()`主要在交互式环境中使用，它的输出不稳定，不应在生产环境中使用。

## 17. divmod(*a*, *b*)

参数为整数时，返回`(a // b, a % b)`；参数为浮点数时，返回`(math.floor(a /
b), a % b)`。而且`0 <= abs(a % b) < abs(b)`。

## 18. enumerate(*iterable*, *start=0*)

返回一个 enumerate 对象。示例：

```python
>>> seasons = ['Spring', 'Summer', 'Fall', 'Winter']
>>> list(enumerate(seasons))
[(0, 'Spring'), (1, 'Summer'), (2, 'Fall'), (3, 'Winter')]
>>> list(enumerate(seasons, start=1))
[(1, 'Spring'), (2, 'Summer'), (3, 'Fall'), (4, 'Winter')]
```

等价于：

```python
def enumerate(sequence, start=0):
    n = start
    for elem in sequence:
        yield n, elem
        n += 1
```

## 19. eval(*expression*, *globals=None*, *locals=None*)

估值表达式。`globals`必须是一个字典，而`locals`可以是任何 mapping 对象。

如果 globals 和 locals 都省略，则使用当前环境的符号表。示例：

```python
>>> x = 1
>>> eval('x+1')
2
```

此函数也能用来执行任何 code 对象（比如使用 `compile()`创建的 code 对象）。但如果这个 code 对象在编译时的`mode` 是 `'exec'`，那么`eval()`将返回`None`。

> 提示：动态执行语句可以使用`exec()`。`globals()`和`locals()`函数分别返回当前的全局和局部符号表，可以用来传入`eval()`和`exec()`。

## 20. exec(*object*[, *globals*[, *locals*]])

动态执行 Python 代码。其中 object 必须是字符串或 code 对象。示例：

```python
>>> program = 'a = 5\nb=10\nprint("Sum =", a+b)'
>>> exec(program)
Sum = 15
```

也可以用于获取用户的输入程序：

```python
program = input('Enter a program:')
exec(program)
```

想要限制用户能访问的对象或函数，可以传入 globals，示例：

```python
>>> from math import *
>>> exec('print(dir())', {'squareRoot': sqrt, 'pow': pow})
['__builtins__', 'pow', 'squareRoot']

# object can have squareRoot() module
>>> exec('print(squareRoot(9))', {'squareRoot': sqrt, 'pow': pow})
3.0
```

## 21. filter(*function*, *iterable*)

`filter()`基于 function 过滤 iterable 的元素，如果 function(element) 返回 `True`，则通过过滤。

`filter()`等价于：

```python
# when function is defined
(element for element in iterable if function(element))

# when function is None
(element for element in iterable if element)
```

`itertools.filterflase()`是这个函数的反函数。

## 22. *class* float([*x*])

构建浮点数。对于一个 Python 对象`x`，`float(x)`会尝试调用`x.__float__()`。示例：

```python
>>> float('+1.23')
1.23
>>> float('   -12345\n')
-12345.0
>>> float('1e-003')
0.001
>>> float('+1E6')
1000000.0
>>> float('-Infinity')  # “inf”, “Inf”, “INFINITY” 和 “iNfINity” 都行，不区分大小写
-inf
```

## 23. format(*value*[, *format_spec*])

将 *value* 转换为格式化后的表示，由 *format_spec* 控制。

*format_spec* 的格式为：

```
[[fill]align][sign][#][0][width][,][.precision][type]
where, the options are
fill        ::=  any character
align       ::=  "<" | ">" | "=" | "^"
sign        ::=  "+" | "-" | " "
width       ::=  integer
precision   ::=  integer
type        ::=  "b" | "c" | "d" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "o" | "s" | "x" | "X" | "%"
```

数字对齐 align 符号含义：

| Type | Meaning                      |
| ---- | ---------------------------- |
| <    | 左对齐                       |
| ^    | 居中对齐                     |
| >    | 右对齐                       |
| =    | 强制符号(+)(-)到最左边的位置 |

数字类型 type 符号函数

| Type | Meaning                                                |
| ---- | ------------------------------------------------------ |
| d    | 十进制整数                                             |
| c    | 对应的 Unicode 字符                                    |
| b    | 二进制格式                                             |
| o    | 八进制格式                                             |
| x    | 十六进制格式                                           |
| n    | 与`d`相同，只是它的数字分隔符因区域设置而不同          |
| e    | 科学标识法 (e)                                         |
| E    | 科学表示法 (E)                                         |
| f    | 显示定点数 （默认为 6 位小数）                         |
| F    | 与`f`相同，只是它将`inf`显示为`INF`，`nan`显示为`NAN`  |
| g    | 通用格式，将数字四舍五入到 p 位有效数字（默认为 6 位） |
| G    | 与`g`相同，只是会显示`E`而不是`e`                      |
| %    | 将数字乘以100并且在后面增加一个%                       |

示例 1：

```python
# d, f and b are type

# integer
>>> print(format(123, "d"))
123

# float arguments
>>> print(format(123.4567898, "f"))
123.456790

# binary format
>>> print(format(12, "b"))
1100
```

示例 2：“对齐”和“填充”：

```python
# integer 
>>> print(format(1234, "*>+7,d"))
*+1,234

# float number
>>> print(format(123.4567, "^-09.3f"))
0123.4570
```

上例相当于`'{:^-09.3f}'.format(123.4567)`。关于 Python String Format，参看 [Python String format()](https://www.programiz.com/python-programming/methods/string/format)

示例 3：重写`__format__()`

```python
# custom __format__() method
class Person:
    def __format__(self, format):
        if(format == 'age'):
            return '23'
        return 'None'

print(format(Person(), "age"))
```

输出为 `23`。

## 24. *class* frozenset([*iterable*])

返回一个 [frozenset](https://docs.python.org/3/library/stdtypes.html#frozenset) 对象。

## 25. getattr(*object*, *name*[, *default*])

 `getattr(x, 'foobar')` 等价于`x.foobar`。如果属性 *name* 不存在，则会尝试返回 *default*，若没有提供 *default* 则会引发 `AttributeError`。

## 26. globals()

返回当前全局符号表的字典。这个字典总是当前模块的符号表（在函数或方法中，会返回函数或方法被定义的模块的全局符号表，而非调用模块的全局符号表）。

## 27. hasattr(*object*, *name*)

如果 *object* 有属性 *name*，则返回 `True`，否则返回`False`。这个函数是由`getattr()`实现的（测试是否引发异常）。

## 28. hash(*object*)

返回一个对象的哈希值（如果有的话）。哈希值是整数。

这个函数主要用来快速比较两个对象。在 set 和 dict 的实现中也用到了，因为 set 和 dict 实际上就是可变大小的哈希表。而 list 不是，因此在 list 中查找元素比在 set 和 dict 中查找要慢很多。参考 [StackOverflow](https://stackoverflow.com/questions/17585730/what-does-hash-do-in-python)

> 特殊：-1 的哈希值是 -2，因为 CPython 里 -1 用来返回错误，所以 -1 的哈希值被改成了 -2。所以`hash(-1)==hash(-2)`。参考 [StackOverflow](https://stackoverflow.com/questions/7648129/when-is-a-python-objects-hash-computed-and-why-is-the-hash-of-1-different)。

## 29. help([*object*])

显示帮助。用于交互式环境中使用。参数可以是字符串或对象。

## 30. hex(*x*)

将整数`x`转换为十六进制字符串（以“0x”开头）。如果`x`不是一个 Python `int`对象，它需要定义一个`__index__()`方法来返回一个整数。示例：

```python
>>> hex(255)
'0xff'
>>> hex(-42)
'-0x2a'
```

也可以用`format()`和字符串格式字面量`f"xxx"`来完成：

```python
>>> '%#x' % 255, '%x' % 255, '%X' % 255
('0xff', 'ff', 'FF')
>>> format(255, '#x'), format(255, 'x'), format(255, 'X')
('0xff', 'ff', 'FF')
>>> f'{255:#x}', f'{255:x}', f'{255:X}'
('0xff', 'ff', 'FF')
```

参看 int() 将十六进制字符串使用16为基数转换为整数。

## 31. id(*object*)

返回对象的 id（一个整数）。

**CPython 实现**：这是对象在内存中的虚拟地址。

## 32. input([*prompt*])

显示 *prompt*，获取用户输入字符串，并去除结尾的回车。如果读到 EOF，则会引发`EOFError`。

> 如果 引入了`readline`模块，`input()`会使用它来提供更多特性，如行编辑和历史记录。

## 33. *class* int([*x*]), int(*x*, *base=10*)

从一个数字或字符串构建一个整数。如果没有参数，则返回 0。

实际上能使用的 *base* 包括：0，2-36。

## 34. isinstance(*object*, *classinfo*)

如果 *object* 是 *classinfo* 的实例或子类的示例，则返回 `True`。如果 *classinfo* 是一个元组，那么只要 *object* 是其中一个 class 的实例或子类的实例，也返回 `True`。

## 33. issubclass(*class*, *classinfo*)

基本同上。注意：类是自己的子类。

## 34. iter(*object*[, *sentinel*])

返回一个 iterator 对象。第一个参数的类型取决于是否存在 *sentinel*。

若没有 *sentinel*，*object* 必须支持迭代协议（实现 `__iter__()`）或序列协议（实现从0开始的`__getitem()__`），否则将会引发`TypeError`。

若有 *sentinel*，那么 *object* 必须是可调用的对象。这种情况下，调用这个所返回的迭代器的`__next__()`会直接调用这个对象（没有参数）直到它返回的值等于 *sentinel*，此时`StopIteration`会被引发，其他情况下是直接返回值。

一个很有用的应用是使用`iter()`来构建一个“块读取器”，例如从一个二进制数据库中读取固定 block 大小的数据：

```python
from functools import partial
with open('mydata.db', 'rb') as f:
    for block in iter(partial(f.read, 64), ''):
        process_block(block)
```

## 35. len(*s*)

返回对象的长度。对象可以是一个序列（string/bytes/tuple/list/range）或一个合集（dict/set/frozen set）。

## 36.*class* list([*iterable*])

返回一个列表。

## 37. locals()

参考 `globals()`。

## 38. map(*function*, *iterable*, *...*)

返回一个 map 对象（迭代器），迭代结果为：将 *function* 施加于 *iterable* 的每个元素上所产生的返回值。实例：

```python
def calculateSquare(n):
  return n*n

numbers = (1, 2, 3, 4)
result = map(calculateSquare, numbers)
print(result)

# converting map object to set
numbersSquare = set(result)
print(numbersSquare)
```

输出为：

```
<map object at 0x7f722da129e8>
{16, 1, 4, 9}
```

## 39. max(*iterable*, \*[, *key*, *default*]), max(*arg1*, *arg2*, **args*[, *key*])

返回 *iterable* 最大的元素或两个以上参数中最大的。

如果 *iterable* 是空的，则尝试返回 *default*。

如果多个元素最大，则返回第一个遇到的。

## 40. memoryview(*obj*)

返回一个 *memory view* 对象。详见 [Memory View](https://docs.python.org/3/library/stdtypes.html#typememoryview)。

## 41. min(*iterable*, \*[, *key*, *default*]), min(*arg1*, *arg2*, **args*[, *key*])

参见 `max()`。

## 42. next(*iterator*[, *default*])

调用 *iterator* 的`__next__()`来获得一个返回值。若 *iterator* 已经耗尽并且提供了 *default*，则会返回 *default*。否则会引发`StopIteration`。

## 43. *class* object()

返回一个空白的对象。`object`是所有类的基类。

> **注意**：`object`没有`__dict__`，所以不能给`object`对象任意增加属性。
>
> `__dict__`是一个存储对象**可写属性**的字典。

## 44. oct(*x*)

将整数`x`转换为八进制字符串（以“0o”开头）。如果`x`不是一个 Python `int`对象，它需要定义一个`__index__()`方法来返回一个整数。示例：

```python
>>> oct(8)
'0o10'
>>> oct(-56)
'-0o70'
```

也可以用`format()`和字符串格式字面量`f"xxx"`来完成：

```python
>>> '%#o' % 10, '%o' % 10
('0o12', '12')
>>> format(10, '#o'), format(10, 'o')
('0o12', '12')
>>> f'{10:#o}', f'{10:o}'
('0o12', '12')
```

## 45. open(*file*, *mode='r'*, *buffering=-1*, *encoding=None*, *errors=None*, *newline=None*, *closefd=True*, *opener=None*)

打开一个文件，返回一个 file 对象。如果打不开，将会引发 `OSError`。

***file*** 可以是一个字符串，或是一个文件描述符 (*file descriptor*)。

不同 ***mode*** 的含义表：

| Character | Meaning                                               |
| --------- | ----------------------------------------------------- |
| `r`       | 读（默认）                                            |
| `w`       | 写，首先会截断（覆盖）之前的文件                      |
| `x`       | 排斥地(exclusively)创建文件，如果之前存在文件则会失败 |
| `a`       | 写，在原文件后增添 (append)                           |
| `b`       | 二进制模式                                            |
| `t`       | 文本模式（默认）                                      |
| `+`       | 更新文件（读写）                                      |

Python 区分二进制 I/O 和文本 I/O。二进制 I/O 将内容返回为 `bytes` 对象，而文本 I/O 将内容返回为 `str `对象。

***buffering*** 参数是一个可选整数，用来设置缓冲策略。传入 0 将缓冲关闭（仅在二进制模式下允许）；传入 1 选择行缓冲（仅在文本模式下有效）；传入大于 1 的数字用来指定缓冲 trunk 的大小。若不指定 *buffering*，默认情况为：

- 二进制文件使用固定大小的 trunk 来缓冲，trunk 的大小取决于设备的 block 的大小，在大多数系统上是 4096 或 8192。
- “交互式”文本模式（`isatty()`返回`True`的文件）使用行缓冲。其他文本文件使用二进制文件的策略。

***encoding*** 指定文件的编码方式，只能在文本模式下使用。默认的编码方式取决于系统（可以通过`locale.getpreferredencoding()`查看）。参看`codec`模块来查看支持的编码方式。

***errors*** 用来指定发生编解码错误时如何处理，只能在文本模式下使用。参看 [`codec` Error Handlers](https://docs.python.org/3/library/codecs.html#error-handlers)。

> 为了简化和标准化错误处理，Python 中的 codecs 通常都会接受一个 errors 参数来指定错误处理策略。

***newline*** 控制如何解释”新行“，仅在文本模式下有效。

读取文件时：

| *newline*                | 含义                                                         |
| ------------------------ | ------------------------------------------------------------ |
| `None`（默认）           | Universal newline 模式被启用：输入可以是`\r`、`\n`或`\r\n`，并且它们都被翻译成`\n`再被读取出来。 |
| `' '`                    | Universal newline 模式被启用：输入可以是`\r`、`\n`或`\r\n`，但它们被原封不动地读取。 |
| `'\r'`、`'\n'`或`'\r\n'` | 仅仅适配一种换行符。                                         |

写入文件时：

| *newline*                | 含义                                                   |
| ------------------------ | ------------------------------------------------------ |
| `None`（默认）           | 任何`\n`字符都被翻译为系统默认换行符（`os.linesep`）。 |
| `' '`                    | 写入`\n`。                                             |
| `'\r'`、`'\n'`或`'\r\n'` | 写入对应的换行符。                                     |

***closefd*** ：如果  *file* 是一个字符串，那么 *closefd* 只能是 `False`，否则将引发错误；如果 *file* 是一个文件描述符，那么 *closefd* 可以被设置为 `True`，此时这个文件描述符对应的文件即使被关闭了也仍然保持打开。

***opener*** ：一个 callable，要被打开的文件会使用 `opener(file, flags)` 来打开，`opener()`需要返回一个打开的文件描述符（比如`os.open()`）。实例：

```python
>>> import os
>>> dir_fd = os.open('somedir', os.O_RDONLY)
>>> def opener(path, flags):
...     return os.open(path, flags, dir_fd=dir_fd)
...
>>> with open('spamspam.txt', 'w', opener=opener) as f:
...     print('This will be written to somedir/spamspam.txt', file=f)
...
>>> os.close(dir_fd)  # don't leak a file descriptor
```

## 46. ord(*c*)

输入一个字符，返回这个字符的 Unicode 码点。例如`ord('a')`返回`97`。

## 47. pow(*x*, *y*[, *z*])

返回 *x* 的 *y* 次幂（模 *z*）。

## 48. print(**objects*, *sep=' '*, *end='\n'*, *file=sys.stdout*, *flush=False*)

所有的非关键词参数都通过`str()`被转换成字符串，以 *sep* 分隔，以 *end* 结尾，并将字符串输出到 *file*。***flush*** 设为`True`能够强制冲洗缓冲区。

## 49. *class* property(*fget=None*, *fset=None*, *fdel=None*, *doc=None*)

参考我的[这篇文章](https://blog.drg1998.cn/posts/python-property/)。

## 50. range(*stop*), range(*start*, *stop*[, *step*])

返回一个 range 对象。参考 [Sequence Types — list, tuple, range](https://docs.python.org/3/library/stdtypes.html#typesseq)。

## 51.repr(*object*)

返回一个代表 *object* 的字符串。对于许多类型，比如内置类型，`repr()`返回的字符串放到`eval()`里能直接返回那个对象；对于其他类型，它一般返回一个字符串，两边是尖括号，里面包括对象的类型和额外信息（比如名字和内存地址）。一个类可以通过重写`__repr__()`方法来控制`repr()`的输出。

## 52. reversed(*seq*)

返回一个逆序的迭代器。*seq* 必须是一个拥有`__reversed__()`方法的对象，或者它支持*序列协议*（拥有`__len__()`方法和从 0 作为参数开始的`__getitem__()`方法）。

## 53. round(*number*[, *ndigits*])

返回 *number* 在小数点后舍入到 *ndigits* 精度的结果。如果省略 *ndigits*，则返回一个舍入后的整数。

注意：舍入方法**不是四舍五入，而是靠近最近的偶数。**举例：

```python
>>> round(0.5)
0
>>> round(-0.5)
0
>>> round(1.5)
2
```

对于一个普通的对象，`round()`会尝试调用对象的`__round__()`方法。

> `round()`对浮点数的行为有时会让人琢磨不透：`round(2.675, 2)`按理说应该返回`2.68`，但实际上它返回`2.67`。原因还是 Python 的浮点精度问题。

## 54. *class* set([*iterable*])

返回一个 `set` 对象。参看 [Set Types — set, frozenset](https://docs.python.org/3/library/stdtypes.html#types-set)。

## 55. setattr(*object*, *name*, *value*)

和 `getattr()` 搭配使用。`setattr(x, 'foobar', 123)` 相当于
`x.foobar = 123`。

## 56. *class* slice(*stop*), slice(*start*, *stop*[, *step*])

返回一个 slice 对象，用于对序列切片，代表一个在 `range(start, stop, step)`的下标。Python 的*扩展下标语法* (在 Python 2.3 引入）也可以生成一个 slice 对象。示例：

```python
>>> class C(object):
...     def __getitem__(self, val):
...         print val
... 
>>> c = C()
>>> c[1:2,3:4]
(slice(1, 2, None), slice(3, 4, None))
>>> c[5:6,7]
(slice(5, 6, None), 7)
```

## 57. sorted(*iterable*, *\**, *key=None*, *reverse=False*)

返回一个排序后的 *iterable* （升序）。*key* 是一个 callable，可以用来对复杂对象指定“按什么排序”。

这个函数保证是稳定的，即对于“相等”的两个元素，它们的相对位置不会改变。

## 58. @staticmethod

将一个方法转换为静态方法。

静态方法不接收第一个 implicit 参数（比如 `self`和`cls`）。使用方法为：

```python
class C:
    # 使用装饰器
    @staticmethod
    def f(arg1, arg2, ...): ...
    
    # 不使用装饰器
    builtin_open = staticmethod(open)
```

注意它的 *classmethod* 的区别：*classmethod* 可以用来访问和更改这个类的内部状态和属性，但 *staticmethod* 不能；*staticmethod* 的主要作用是**把一些和某个类相关的工具函数放到这个类的命名空间下**。参看 [class method vs static method in Python](https://www.geeksforgeeks.org/class-method-vs-static-method-python/)。

## 59. *class* str(*object=''*), str(*object=b''*, *encoding='utf-8'*, *errors='strict'*)

返回一个字符串对象。

## 60. sum(*iterable*[, *start*])

将 *iterable* 的元素求和，再加上 *start* ，得到总和并返回。*start* 默认为 0。通常用于整数类型求和。

对于其他类型有更好的求和方法，比如串联字符串使用`''.join(sequence)`最快；用扩展精度求和浮点数，使用`math.fsum()`；将 iterable 串联起来可以使用`itertools.chain()`。

## 61. super([*type*[, *object-or-type*]])

返回一个代理对象，它能把对方法的调用传递给 *type* 的父类或兄弟类。

一个类的`__mro__`动态地记录了“方法解析搜索顺序” （Method Resuolution search Order），像是一个继承链，`getattr()`和`super()`使用`__mro__`来解析对方法的访问。例如：

```python
>>> class myList(list):
...     x = 1

>>> myList.__mro__
(<class '__main__.myList'>, <class 'list'>, <class 'object'>)
```

`super()`主要有两个用途：

- 避免显式地使用基类
- 用于多重继承

**单继承**例子：

```python
class Mammal(object):
  def __init__(self, mammalName):
    print(mammalName, 'is a warm-blooded animal.')
    
class Dog(Mammal):
  def __init__(self):
    print('Dog has four legs.')
    super().__init__('Dog')
    
d1 = Dog()
```

由于避免直接使用`Mammal.__init__(self, 'Dog')`，有一天改变了`Dog`的基类后代码仍然是可用的。

**多重继承**例子：

```python
class Animal:
  def __init__(self, animalName):
    print(animalName, 'is an animal.');

class Mammal(Animal):
  def __init__(self, mammalName):
    print(mammalName, 'is a warm-blooded animal.')
    super().__init__(mammalName)
    
class NonWingedMammal(Mammal):
  def __init__(self, NonWingedMammalName):
    print(NonWingedMammalName, "can't fly.")
    super().__init__(NonWingedMammalName)

class NonMarineMammal(Mammal):
  def __init__(self, NonMarineMammalName):
    print(NonMarineMammalName, "can't swim.")
    super().__init__(NonMarineMammalName)

class Dog(NonMarineMammal, NonWingedMammal):
  def __init__(self):
    print('Dog has 4 legs.');
    super().__init__('Dog')
    
d = Dog()
print('')
bat = NonMarineMammal('Bat') 
```

输出为：

```
Dog has 4 legs.
Dog can't swim.
Dog can't fly.
Dog is a warm-blooded animal.
Dog is an animal.

Bat can't swim.
Bat is a warm-blooded animal.
Bat is an animal.
```

注意到：一行`super().__init__('Dog')`实际上把它两个基类的`__init__()`都依次调用了，先调用的是第一个基类，再调用第二个基类。

关于`super()`在 Python 多重继承中的问题，参看 [How does Python's super() work with multiple inheritance?](https://stackoverflow.com/questions/3277367/how-does-pythons-super-work-with-multiple-inheritance)

关于 MRO，可以参看 Guido van Rossum 的这一篇文章：[Method Resolution Order](http://python-history.blogspot.com/2010/06/method-resolution-order.html)。

参看 [guide to using super()](https://rhettinger.wordpress.com/2011/05/26/super-considered-super/)。

## 62. tuple([*iterable*])

返回一个 tuple 对象。

## 63. *class* type(*object*), type(*name*, *bases*, *dict*)

返回 *object*  的类型（`object.__class__`）。

通过三个参数，可以动态构建 class。例如下面两种写法等价：

```python
>>> class X:
...     a = 1
...
>>> X = type('X', (object,), dict(a=1))
```

所有“类”的 type 都是 type，包括 type。

## 64. vars([*object*])

返回一个对象的`__dict__`属性。这个对象可以是模块、类、实例等任何对象。

没有参数时，`vars()`和`locals()`等价。

## 65. zip(**iterables*)

将不同 iterables 里的元素融合，返回一个新的 tuple 的迭代器，第 $i$ 个 tuple 是所有传入 iterable 里的第 $i$ 个元素。示例：

```python
>>> x = [1, 2, 3]
>>> y = [4, 5, 6]
>>> zipped = zip(x, y)
>>> list(zipped)
[(1, 4), (2, 5), (3, 6)]
```

`zip()`搭配`*`使用可以用来解压：

```python
>>> x2, y2 = zip(*zip(x, y))
>>> x == list(x2) and y == list(y2)
True
```

## 66. \_\_import\_\_(*name*, *globals=None*, *locals=None*, *fromlist=()*, *level=0*)

这个函数被`import`语句调用。你可以通过重写这个函数来改变`import`语句的行为，只要引入`builtins`模块并对`builtins.__import__`赋值即可。但最好别这么做。你若真想搞点自己的东西，可以看看[`importlib.import_module()`](https://docs.python.org/3/library/importlib.html#importlib.import_module)。

`from spam.ham import eggs, sausage as saus` 实际上执行了

```python
_temp = __import__('spam.ham', globals(), locals(), ['eggs', 'sausage'], 0)
eggs = _temp.eggs
saus = _temp.sausage
```

（本文完）