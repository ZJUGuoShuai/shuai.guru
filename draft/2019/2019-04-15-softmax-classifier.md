---
date: "2019-04-15T00:00:00Z"
tags: 统计学习
title: 理解 Softmax Classifier
toc: true
---

除了 SVM，**Softmax 分类器**也是一个比较常见的分类器，主要用于多分类。它实际上是二分类中的 **Logistic Regression 分类器**的推广。

## 简述

Softmax 分类器和 SVM 的区别主要是 loss 函数不同。SVM 将 MLP[^0] 的输出向量 $f(x_i;W)=Wx_i$ 解释为每个类的 *score*，这不具有直观性；而 Softmax 分类器认为这个输出向量 $f(x_i;W)$是每个类的“未归一化的 log 概率 (unnormalized log probability)”，并且把 SVM 中的 *hinge loss* 改为 ***cross-entropy loss***，即：

$$
L_i = -\log\left(\frac{e^{f_{y_i}}}{ \sum_j e^{f_j} }\right) \hspace{0.5in} \text{or equivalently} \hspace{0.5in} L_i = -f_{y_i} + \log\sum_j e^{f_j}
$$

当然，训练集的经验损失 $L$ 依然是样本损失 $L_i$ 的均值加上正则化项 $R(W)$：

$$
L=\frac{1}{N}\sum_{i=1}^NL_i+R(W)
$$

注：标准 **softmax 函数**定义为：

$$
\sigma:\mathbb{R}^K\mapsto\mathbb{R}^K\\
\sigma(\mathbf{z})_i=\frac{e^{z_i}}{\sum_{j=1}^Ke^{z_j}} \text{ for i=1,...,K}
$$

## 信息论的观点

为什么叫上面那个 loss 为交叉熵 (cross-entropy) 呢？这来自信息论。在信息论中，两个概率分布 $p$ 和 $q$ 的**交叉熵**定义为：

$$
H(p,q)=-\sum_xp(x)\log q(x)
$$

通常 $p$ 是一个“真实”分布，而 $q$ 是根据训练集估计出来的分布，也就是神经网络的输出（概率）。拿到一个训练集，每个样本 $(X,Y)$ 都带有标签 $Y$，因此“真实”分布是一个确定的分布 $p=[0,...1,...,0]$，在第 $Y_i$ 的位置上有个1，其他都是0，所以得到的交叉熵算出来就是 $-\log q(x)$。

还可以从另一个角度看。交叉熵可以用**熵**和**KL散度 (Kullback-Leibler divergence)**来表示：

$$
H(p,q)=H(p)+D_{KL}(p||q)
$$

由于 $p$ 是一个确定性的分布，所以 $H(p)=0​$ ，所以最小化交叉熵等价于**最小化KL散度**。

## 概率论的观点

由于这里的假设模型 (hypothesis) 是条件概率分布 $P_W(Y\mid X)=\sigma (f(X;W))_Y=\frac{e^{f_Y}}{ \sum_j e^{f_j} }​$，即 MLP 后面加一层 softmax；损失函数取的是对数损失函数，即

$$
L(Y, P_W(Y\mid X))=-\log P_W(Y\mid X)
$$

则**极大似然估计 (MLE)** 等价于**经验风险最小化**[^1]：

$$
\min_{f\in \mathcal{F}} R_{emp}(P_W)=\frac{1}{N} \sum_{i=1}^NL(Y, P_W(Y|X))
$$

而如果假设模型的复杂度由模型的先验概率表示，比如模型参数 $W​$ 服从高斯分布，则**最大后验估计 (MAP)** 等价于**结构风险最小化**[^2]：

$$
\min_{f\in \mathcal{F}} R_{emp}(P_W)=\frac{1}{N} \sum_{i=1}^NL(Y, P_W(Y|X))+\lambda J(P_W)
$$

关于这两条的证明可以看我这篇文章：[MLE/MAP 与经验风险、结构风险](https://blog.drg1998.cn/2019/04/16/mle-map-risk.html)。

## 实现时的问题：数值稳定性

由于指数的问题，$e^{f_{y_i}}$ 和 $\sum_j e^{f_j}$ 也许会非常大，这在程序中很要命，因为除以一个非常大的数会有下溢问题。所以要用一个技巧来规避这个问题。注意到，如果在 softmax 分子分母都乘以一个常数 $C$，有下面的等式关系：

$$
\frac{e^{f_{y_i}}}{\sum_j e^{f_j}}
= \frac{Ce^{f_{y_i}}}{C\sum_j e^{f_j}}
= \frac{e^{f_{y_i} + \log C}}{\sum_j e^{f_j + \log C}}
$$

也就是说， $f_i$ 同时增加或减少相同的数值，不会影响到最终的值。所以，为了让分母变小，我们采取 $C$ 的值为 $\log C = -\max_j f_j$，用代码来说明：

```python
f = np.array([123, 456, 789]) # 一个三分类问题，每个输出的 score 都很大
p = np.exp(f) / np.sum(np.exp(f)) # 危险: 可能会有数值下溢问题

# 解决方法: 减去最大值
f -= np.max(f) # f 变成 [-666, -333, 0]
p = np.exp(f) / np.sum(np.exp(f)) # 现在很安全
```

## SVM vs. Softmax

下面这张图很好地说明了 SVM 和 Softmax 分类器的区别：

![](https://cs231n.github.io/assets/svmvssoftmax.png)

## 参考

- <https://cs231n.github.io/linear-classify/#softmax>

[^0]: Multilayer perceptron，多层感知器
[^1]: 李航：《统计学习方法》
[^2]: 李航：《统计学习方法》