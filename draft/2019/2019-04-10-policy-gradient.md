---
date: "2019-04-10T00:00:00Z"
tags: 强化学习
title: Policy Gradient 方法简述
toc: true
---

## 0. 强化学习术语表

公式中大写一般代表随机变量，小写为实例（真实取值）。

| 术语                            | 公式                                                         | 含义                                                         |
| ------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| state                           | $S_t$                                                        | $t$ 时刻 agent 的状态                                        |
| action                          | $A_t$                                                        | $t$ 时刻 agent 的动作                                        |
| reward                          | $R_t$                                                        | $t$ 时刻 agent 获得的奖励                                    |
| transition function / dynamic   | $p(s', r \vert s, a)  \doteq \Pr(S_{t+1} = s', R_{t+1} = r \vert S_t = s, A_t = a)$ | 环境的状态转移函数，agent 从状态 $s$，采取动作 $a$，变成状态 $s'$ 并获得奖励 $r​$ 的概率。它由环境唯一决定，所以也叫环境的动态特性（dynamic）。 |
| reward function                 | $R(s, a) \doteq \mathbb{E} [R_{t+1} \vert S_t = s, A_t = a] = \sum_{r\in\mathcal{R}} r \sum_{s' \in \mathcal{S}} p(s', r \vert s, a)$ | 奖励函数，一般可以人为地设计                                 |
| discounting factor              | $\gamma \in [0, 1]$                                          | 递减因子，为了惩罚未来的 reward                              |
| return                          | $G_t \doteq R_{t+1} + \gamma R_{t+2} + \dots = \sum_{k=0}^{\infty} \gamma^k R_{t+k+1}$ | $t$ 时刻之后的累积递减奖励，也叫 future reward               |
| deterministic policy            | $\pi(s) = a$                                                 | 确定性策略，输入一个状态，输出一个动作                       |
| stochastic policy               | $\pi(a \vert s) \doteq \Pr_\pi (A_t=a \vert S_t=s)$          | 随机性策略，输入一个状态和动作，输出该动作的概率             |
| state-value function            | $V_{\pi}(s) \doteq \mathbb{E}_{\pi}[G_t \vert S_t = s]$      | 从当前状态 $s$ 开始，之后所得 return 的期望                  |
| action-value function / Q-value | $Q_{\pi}(s, a) \doteq \mathbb{E}_{\pi}[G_t \vert S_t = s, A_t = a]$ | 从当前状态 $s$，选择了动作 $a$，之后所得 return 的期望       |
| advantage function              | $A_{\pi}(s, a) \doteq Q_{\pi}(s, a) - V_{\pi}(s)$            | 度量某个动作相对其他动作的优势                               |

本术语表并未对其中的概念进行详细阐述，仅为了后文论述的方便。如果要详细了解这些概念，推荐阅读：

- <https://spinningup.openai.com/en/latest/spinningup/rl_intro.html>。

## I. 背景

强化学习的方法目前主要分为两大类：一类基于 *action-value function*，它们计算每个动作的 *value*，然后选择 *value* 最大的动作，例如 *Q-learning* 算法。还有一类是基于参数化的策略来选择动作，直接通过学习获得一个策略 $\pi_\theta(a \vert s)$，其中 $\boldsymbol{\theta} \in \mathbb{R}^{d'}$ 是策略的参数向量。我们的目标是要求学得一个最优参数 $\boldsymbol{\theta}^\ast$ 使得该策略的**性能度量** $J(\boldsymbol \theta)​$ 取得最大值。这是一个最优化问题，一个自然的想法是用 *Gradient Ascend* 来求解：

$$
\boldsymbol{\theta}_{t+1}=\boldsymbol{\theta}_{t}+\alpha \widehat{\nabla{J(\boldsymbol \theta_t)}}
$$

其中 $\widehat{\nabla{J(\boldsymbol{\theta_t})}}\in \mathbb{R}^{d'}$ 是一个估计，它的数学期望可以逼近 $J$ 对 $\boldsymbol{\theta}_t​$ 的梯度。所有遵循这个范式的方法统称为 ***Policy Gradient*** 方法。

## II. 如何参数化一个策略？

现在考虑一个 policy 如何用参数 $\boldsymbol{\theta}$ 来表示。一般来说，只要 $J$ 能够对 $\boldsymbol{\theta}$ 求梯度即可。在动作空间是离散的情况下，一个常用的做法是利用每个 “state-value pair”的偏好值（preference）$h(s, a, \boldsymbol{\theta})$，这个偏好值 $h$ 越大，则在状态 $s$ 下选择 $a$ 的概率也应该越大。借助 *Softmax* 函数，我们可以将策略定义为：

$$
\pi(a|s,\boldsymbol{\theta})\doteq\frac{e^{h(s, a, \boldsymbol{\theta})}}{\sum_ah(s, a, \boldsymbol{\theta})}
$$

而偏好值 $h$ 可以定义为一个线性函数：

$$
h(s, a, \boldsymbol{\theta})=\boldsymbol{\theta}^\top \mathbf{x}(s,a)
$$

其中 $\mathbf{x}(s,a)​$ 就是一个 *feature vector*，所以如果用神经网络来实现一个 policy 的话，就是“线性全连接层+*Softmax* 激活函数”，非常简单。

## III. 策略梯度定理（Policy Gradient Theorem）

**片段型任务（episodic tasks）**和**连续型任务（continuous tasks）**具有不同的性能度量 $J$，应该要区别对待。但接下来的讨论主要面向**片段型任务**，连续型任务可以在此基础上推导。我们可以这样假设：每一轮任务（episode）都开始于某个起点状态 $s_0$。这会简化数学描述，并且不失一般性（你的任务总得有个起点吧）。

定义性能度量 $J​$ 为初始状态 $s_0​$ 的 *value*（累积回报的期望），即：

$$
J(\boldsymbol{\theta})\doteq V_{\pi_{\boldsymbol\theta}}(s_0)\doteq\mathbb{E}_{\pi_{\boldsymbol\theta}}[G_t \vert S_t = s_0]
$$

求解过程如下（所有 $\pi_{\boldsymbol{\theta}}$ 都简写为 $\pi$，但不要忘记它是由 $\boldsymbol{\theta}​$ 参数化的）

$$
\begin{align}
\nabla V_\pi(s) &= \nabla \Big[\sum_a\pi(a|s)Q(s,a)\Big], \text{ for all }s \in \mathcal{S} \tag{由定义}\\
 &=\sum_a \Big[ \nabla\pi(a|s)Q(s,a)+\pi(a|s)\nabla Q(s,a) \Big] \tag{导数乘积法则}\\
 &=\sum_a \Big[ \nabla\pi(a|s)Q(s,a)+\pi(a|s)\nabla \sum_{s',r}p(s',r|s,a)(r+V_\pi(s')) \Big] \tag{由定义}\\
 &=\sum_a \Big[ \nabla\pi(a|s)Q(s,a)+\pi(a|s)\sum_{s',r}p(s',r|s,a)\nabla V_\pi(s') \Big] \tag{p和r都不是θ的函数}\\
 &=\sum_a \Big[ \nabla\pi(a|s)Q(s,a)+\pi(a|s)\sum_{s'}p(s'|s,a)\nabla V_\pi(s') \Big] \tag{边际概率}\\
 &=\sum_a \Big[ \nabla\pi(a|s)Q(s,a)+\pi(a|s)\sum_{s'}p(s'|s,a)\\
 &\sum_{a'} \big[ \nabla\pi(a'|s')Q(s',a')+\pi(a'|s')\sum_{s''}p(s''|s',a')\nabla V_\pi(s'') \big] \Big] \tag{展开}\\
 &=\sum_{x\in \mathcal{S}}\sum_{k=0}^{\infty}\Pr(s\to x,k,\pi)\sum_a\nabla\pi(a|x)Q_\pi(x,a) \tag{易推得}
\end{align}
$$

其中 $\Pr(s\to x,k,\pi)$ 意为：在策略 $\pi$ 下，从状态 $s$ 通过 $k$ 步转移到状态 $x$ 的概率。于是乎：

$$
\begin{align}
\nabla J(\boldsymbol{\theta})&=\nabla V_{\pi_{\boldsymbol\theta}}(s_0)\\ \tag{接前文证明}
&=\sum_s\Big(\sum_{k=0}^{\infty}\Pr(s_0\to s,k,\pi)\Big)\sum_a\nabla\pi(a|s)Q_\pi(s,a)\\ \tag{定义}
&=\sum_s\eta(s)\sum_a\nabla\pi(a|s)Q_\pi(s,a)\\
&=\sum_{s'}\eta(s')\sum_s\frac{\eta(s)}{\sum_{s'}\eta(s')}\sum_a\nabla\pi(a|s)Q_\pi(s,a)\\
&=\sum_{s'}\eta(s')\sum_s\mu(s)\sum_a\nabla\pi(a|s)Q_\pi(s,a)\\
&\propto \sum_s\mu(s)\sum_a\nabla\pi(a|s)Q_\pi(s,a) \tag{Q.E.D}
\end{align}
$$

这样便得到了**策略梯度定理**：

$$
\nabla J(\boldsymbol{\theta})\propto \sum_s\mu(s)\sum_a\nabla\pi(a|s)Q_\pi(s,a)
$$


## 参考

- <https://lilianweng.github.io/lil-log/2018/02/19/a-long-peek-into-强化学习.html>