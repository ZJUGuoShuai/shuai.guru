---
date: "2019-04-15T00:00:00Z"
tags: 统计学习
title: MLE/MAP 与经验风险、结构风险
---

李航的《统计学习方法》一书的第一章概论中有下面两句话，我当时看的时候没有理解，于是自己推导了一下。

## 一

>  当模型是**条件概率分布**，损失函数是**对数损失函数**时，经验风险最小化就等价极大似然估计。

首先复习**极大似然估计** (MLE)。给定一组采样自分布 $P_\theta(X)$ 的**独立同分布**的观测数据 $x_1,x_2, \dots, x_n$，想要估计分布中的参数 $\theta$。首先得到这组观测发生的**对数似然** $\ell$：

$$
\begin{align}
\ell(\theta\,;\boldsymbol{x}) &=  \log\mathcal{L}(\theta\,;\boldsymbol{x})\\
&=\log P_\theta(X=\boldsymbol{x})\\
&=\log \prod_iP_\theta(x_i)\\
&=\sum_i\log P_\theta(x_i)
\end{align}
$$

为了使这组观测发生的可能性最大，则 $\theta​$ 的极大似然估计值为：

$$
\begin{align}
\hat{\theta}_{\mathrm{MLE}}&=\argmax_\theta \sum_i\log P_\theta(x_i)\\
&=\argmax_\theta \frac{1}{N}\sum_i\log P_\theta(x_i)
\end{align}
$$

<hr>

**证明：**假设模型为条件概率分布 $P_{\boldsymbol{\theta}}(Y\mid X)$。给定采样自此分布的一个训练集：

$$
T=\{(x_1,y_1),\dots,(x_N,y_N)\}
$$

则它的对数似然 $\ell$ 为：

$$
\begin{align}
\ell(\boldsymbol{\theta}\,;\boldsymbol{y}\mid\boldsymbol{x}) &=  \log\mathcal{L}(\boldsymbol{\theta}\,;\boldsymbol{y}\mid\boldsymbol{x})\\
&=\log P_{\boldsymbol{\theta}}(Y=\boldsymbol{y}\mid X=\boldsymbol{x})\\
&=\log \prod_iP_{\boldsymbol{\theta}}(y_i\mid x_i)\\
&=\sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)
\end{align}
$$

则 $\boldsymbol{\theta}$  的估计值为：

$$
\begin{align}
\hat{\boldsymbol{\theta}}_{\mathrm{MLE}}&=\argmax_{\boldsymbol{\theta}} \sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)\\
&=\argmax_{\boldsymbol{\theta}} \frac{1}{N}\sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)\\
&=\argmin_{\boldsymbol{\theta}} \frac{1}{N}\sum_i-\log P_{\boldsymbol{\theta}}(y_i\mid x_i)\\
&=\argmin_{\boldsymbol{\theta}} \frac{1}{N}\sum_iL(y_i,P_{\boldsymbol{\theta}}(y_i\mid x_i))\\
&=\argmin_{\boldsymbol{\theta}} R_{\mathrm{emp}}(P_{\boldsymbol{\theta}})\\
\end{align}
$$

其中 $L(y_i,P(y_i\mid x_i))=-\log P_{\boldsymbol{\theta}}(y_i\mid x_i)​$ 就是标准的对数损失函数。所以 MLE 在这里等价于最小化经验风险 $R_{\mathrm{emp}} \blacksquare​$

## 二

> 当模型是**条件概率分布**，损失函数是**对数损失函数**、模型复杂度由模型的**先验概率**表示时，结构风险最小化就等价最大后验概率估计。

首先复习**最大后验概率估计** (MAP)。由于 MAP 涉及两个概率分布，所以这里使用不同的记号。记

$$
f:x\mapsto f(x\mid\theta)
$$

是 $X$ 的概率密度函数，则

$$
f:\theta\mapsto f(x\mid\theta)
$$

是 $\theta$  的似然函数。现在假设 $\theta$ 的先验分布 $g$ 存在，则由**贝叶斯定理**，我们可以计算 $\theta$ 的后验分布：

$$
\theta\mapsto f(\theta\mid x)=\frac{f(x\mid\theta)g(\theta)}{\int_\Theta f(x\mid v)g(v)dv}
$$

其中 $\Theta$ 是 $g$ 的定义域。则 $\theta$  的估计值为：

$$
\hat{\theta}_{\mathrm{MAP}}(x)
= \underset{\theta}{\operatorname{arg\,max}} \ f(\theta \mid x)
= \underset{\theta}{\operatorname{arg\,max}} \ \frac{f(x \mid \theta) \, g(\theta)}
  {\displaystyle\int_{\Theta} f(x \mid \vartheta) \, g(\vartheta) \, d\vartheta}
= \underset{\theta}{\operatorname{arg\,max}} \ f(x \mid \theta) \, g(\theta)
$$

对比 MLE：

$$
\hat{\theta}_{\mathrm{MLE}}(x)
=\underset{\boldsymbol{\theta}}{\operatorname{arg\,max}} \ f(x \mid \boldsymbol{\theta})
$$

所以 MAP 只是增加了一个先验概率分布的因子 $g(\theta )​$。

注 1：MLE 实际上只是 MAP 的一个特殊情形——MLE 是参数的先验概率为均匀分布[^0]时的 MAP。

注 2：我认为 $f(x\mid\theta )$  和 $f(x;\theta )$ 实际上语义相同，可以互换。

<hr>

**证明：**假设模型为条件概率分布 $P_{\boldsymbol{\theta}}(Y\mid X)$。给定采样自此分布的一个训练集：

$$
T=\{(x_1,y_1),\dots,(x_N,y_N)\}
$$

则 $\boldsymbol{\theta}​$ 的最大后验概率估计为：

$$
\begin{align}
\hat{\boldsymbol{\theta}}_{\mathrm{MAP}}(\boldsymbol{x},\boldsymbol{y})&= \argmax_{\boldsymbol{\theta}} \ \log \big[ P(\boldsymbol{y}\mid\boldsymbol{x} ; \boldsymbol{\theta}) \, g(\boldsymbol{\theta}) \big]\\
&=\argmax_{\boldsymbol{\theta}}\ \log \big[g(\boldsymbol{\theta})\prod_iP_{\boldsymbol{\theta}}(y_i\mid x_i)\big]\\
&=\argmax_{\boldsymbol{\theta}}\ \sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)+\log g(\boldsymbol{\theta})\\
&=\argmax_{\boldsymbol{\theta}}\ \frac{1}{N}\sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)+\frac{\log g(\boldsymbol{\theta})}{N}
\end{align}
$$

假设 $\boldsymbol{\theta}​$ 具有先验分布  $\exp\{-\lambda N\parallel\boldsymbol{\theta}\parallel_2^2\}​$，则上式等于

$$
\begin{align}
\hat{\boldsymbol{\theta}}_{\mathrm{MAP}}&=\argmax_{\boldsymbol{\theta}}\ \frac{1}{N}\sum_i\log P_{\boldsymbol{\theta}}(y_i\mid x_i)-\lambda \parallel\boldsymbol{\theta}\parallel_2^2\\
&=\argmin_{\boldsymbol{\theta}}\ \frac{1}{N}\sum_i-\log P_{\boldsymbol{\theta}}(y_i\mid x_i)+\lambda \parallel\boldsymbol{\theta}\parallel_2^2\\
&=\argmin_{\boldsymbol{\theta}}\ \frac{1}{N}\sum_iL_i+\lambda J\\
&=\argmin_{\boldsymbol{\theta}} R_{\mathrm{srm}}(P_{\boldsymbol{\theta}})\\
\end{align}
$$

所以 MLE 在这里等价于最小化经验风险 $R_{\mathrm{srm}} \blacksquare$

[^0]: <https://wiseodd.github.io/techblog/2017/01/01/mle-vs-map/>