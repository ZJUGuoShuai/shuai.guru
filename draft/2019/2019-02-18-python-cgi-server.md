---
date: "2019-02-18T00:00:00Z"
tags: Python
title: 从 Python 快速启动 CGI 服务器
---

很多人知道 Python 3 可以快速启动一个 HTTP 服务器：

```
$ python3 -m http.server 8000
```

今天我查阅 `http.server` 模块文档时发现它支持运行 CGI 脚本，只要加上 `--cgi` 选项。

入门 Web 后端的初学者一定是要学习 CGI 的（不管是学历史还是学概念都有好处），而配置 Apache / NGINX 环境对他们来说可能比较困难。我发现用 Python 这种自带的基础服务器既方便又简单。

## 例子

我们来写一个 Hello World 的 Python CGI 程序 `hello.py` 放在 `cgi-bin/` 里：

```python
#!/usr/bin/python3
print('Content-type: text/html')
print()
print('Hello world!')
```

再写一个前端网页 `index.html`：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello Test</title>
</head>
<body>
    <form action="/cgi-bin/hello.py" method="post">
        <button type="submit">Hello</button>
    </form>
</body>
</html>
```

整个目录结构为：

```
cgi-bin/
	hello.py
index.html
```

然后将 CGI 脚本赋予运行权限：

```
$ chmod a+x cgi-bin/hello.py
```

## 运行测试

运行

```
$ python3 -m http.server --cgi 8000
```

浏览器访问 `http://127.0.0.1:8000/`：

![图片](https://i.loli.net/2019/02/18/5c6a2bbc94fe7.png)

点击按钮：

![图片](https://i.loli.net/2019/02/18/5c6a2bfe4fd82.png)

（本文完）