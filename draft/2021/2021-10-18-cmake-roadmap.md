---
date: "2021-10-18T00:00:00Z"
tags: CMake
title: CMake 官方文档阅读指南
toc: true
---

[CMake 官方文档](https://cmake.org/cmake/help/latest/index.html)又多又难懂，新人容易迷失。因此这里总结一下阅读/学习路线，供 CMake 初学者参考。

## Level 1 - 软件用户

如果你使用 CMake 的原因仅仅是为了**编译某个（通过源码发行的）软件**，那你只用看 [**用户交互指南（User Interaction Guide）**](https://cmake.org/cmake/help/latest/guide/user-interaction/index.html) 就可以了。这个文档告诉你如何
1. 使用 `cmake` 命令行程序或 `cmake-gui` 图形界面程序；
2. 指定**生成器**（generator），即**原生构建工具**（Unix Makefiles、Ninja 或 Visual Studio），同时对软件进行配置；
3. 最终完成软件的构建。

这个阶段基本不需要学习 CMake 语言，只需学会如何使用 CMake 程序来构建软件。

## Level 2.A - 软件开发者：使用 CMake 作为自己软件的构建工具

如果你使用 CMake 的原因是为了让自己的软件使用 CMake 作为构建工具，那你可以从 [**CMake 教程（CMake Tutorial）**](https://cmake.org/cmake/help/latest/guide/tutorial/index.html) 开始。它教你如何：

1. 编写最基本的项目构建的配置文件——`CMakeLists.txt`（还包括如何设置项目版本号、C++ 标准等）；
2. 引入外部依赖；
3. 自动安装或测试编译好的程序；
4. 自动打包编译好的程序；
5. Debug 模式与 Release 模式；
6. ……

这个教程很长，是一个完整的项目，建议按照教程一步步动手敲命令和代码，虽然花时间，但收获远大于仅仅阅读教程。

## Level 2.B - 软件开发者：使用外部依赖

一个项目免不了要使用外部依赖（第三方库等）。如果想较为深入地研究这块内容，最好在前面的基础上再去阅读 [**依赖使用指南（Using Dependencies Guide）**](https://cmake.org/cmake/help/latest/guide/using-dependencies/index.html)。它较为全面地介绍了在 CMake 中如何通过不同的方式引入外部依赖，以及这些方法之间的区别等。

## Level 3 - CMake 爱好者

CMake 博大精深，学习完前面的内容只能算入门，随着时间的推进你一定会产生许多不理解的问题。这时你可以尝试阅读 [**CMake 构建系统手册（cmake-buildsystem）**](https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html)，这个手册内容更多，更有理论性，读完它你将对 CMake 有更加深入的理解。

## Level 4 - CMake 大师

如果你想要将你基于 CMake 构建的基础库或软件发布出去让其他人使用，那么你必须按照 CMake 的设计哲学来“打包”你的软件，让其他用户可以开开心心地使用的你的库或软件。这时候你应该阅读 [**CMake 打包手册（cmake-packages）**](https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html)，按照这个文档的内容来打包，才能让用户更容易使用你的包。
