---
date: "2022-02-15T00:00:00Z"
tags: CUDA
title: 'CUDA Memory (1): Zero-Copy Memory'
---

使用**零拷贝内存**，我们可以**直接从 GPU 访问 host 上的内存**，不需要手动将内存拷贝到 GPU 上。

# 1. 使用方法

使用 `cudaHostAlloc()` 进行内存分配，并在 `flags` 中传入 `cudaHostAllocMapped`（或加上`cudaHostAllocWriteCombined`）。

## 解释：

- `cudaHostAlloc()` 的作用：分配**锁页内存**（page-locked memory）；
- `cudaHostAllocMapped` 的意思：将所分配内存的地址空间映射到 CUDA 地址空间中，这样 GPU 就可以直接访问 host 上的这块儿内存了，即**零拷贝**。
- `cudaHostAllocWriteCombined` 的意思：将内存分配为 *Write-Combined* 类型的（WC 内存）。在有些计算机系统中，WC 内存在 PCI 总线上的传输速度更快，但从 CPU 上访问却更慢。因此 WC 内存适用于“CPU 写入，GPU 读取”的使用场景，即 Host → Device。

## 注意：

- 使用 `cudaHostAlloc()` 分配的内存需要用 `cudaHostFree()` 来释放。
- `cudaHostAlloc()` 返回的是 host 上的地址，需要用 `cudaHostGetDevicePointer()` 将其转换为 device 上的地址。

# 2. 性能（什么时候用？）

- **集成显卡**：👍 一定能提高性能，因为锁页内存避免了“两次拷贝”的问题（pagable memory → page-locked memory → GPU memory）。
- **独立显卡**：
    - 一次读写：👍 也能提高性能，理由同上；
    - 多次读写：👎 零拷贝内存没有被缓存到 GPU 上，因此不如使用 shared momory 或拷贝到 global memory，直接避免多次 CPU 与 GPU 之间的数据搬运。
- **No-Free-Lunch**：分配过多的锁页内存会占用 host 端的物理内存空间，可能会降低整个系统的性能。因此锁页内存的使用场景主要是 host 和 devices 之间数据拷贝的**暂存区**（staging area）。

# 参考

- Sanders, Jason, and Edward Kandrot. ***CUDA by example: an introduction to general-purpose GPU programming***. Addison-Wesley Professional, 2010.