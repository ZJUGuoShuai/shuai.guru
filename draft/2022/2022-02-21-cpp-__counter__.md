---
date: "2022-02-21T15:45:51Z"
tags: C++
title: GCC 中预定义宏 __COUNTER__ 的作用
toc: true
---

在 [GCC 4.3 的 Release Notes](https://gcc.gnu.org/gcc-4.3/changes.html) 中有解释：

> A new predefined macro `__COUNTER__` has been added. It expands to sequential integral values starting from 0.  In conjunction with the `##` operator, this provides a convenient means to generate unique identifiers.
> 

即 `__COUNTER__` 宏的作用就是展开为一个整数，这个整数会从 0 开始，不重复。

其作用**主要是配合 `##` 操作符，生成唯一标识符**。

# 示例程序

```cpp
#include <iostream>

#define _STR(x) #x
#define STR(x) _STR(x)

#define _CONCAT(a, b) a##b
#define CONCAT(a, b) _CONCAT(a, b)

#define UNIQUE_NAME(name) STR(CONCAT(name, __COUNTER__))

int main() {
  std::cout << UNIQUE_NAME(Apple_) << "\n";
  std::cout << UNIQUE_NAME(Apple_) << "\n";
  std::cout << UNIQUE_NAME(Apple_) << "\n";
  std::cout << UNIQUE_NAME(Apple_) << "\n";
  return 0;
}
```

# 输出结果

```
Apple_0
Apple_1
Apple_2
Apple_3
```

至于为什么定义 `_STR` 和 `STR`，而非直接定义一个 `STR`（以及 `_CONCAT` 和 `CONCAT`），请参考我的另一篇文章：[GCC 预处理：如何正确地将宏展开的结果转成字符串](/2022/02/21/cpp-stringify-macro-with-expansion.html)。