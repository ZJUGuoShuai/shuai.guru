---
date: "2022-02-21T00:00:00Z"
tags: C++
title: 'C/C++ 预处理操作符：# 和 ##'
toc: true
---

# 一、字符串化：`#` 操作符

`#` 操作符的作用是将**宏参数**转换为**字符串字面量**，即**使用双引号（**`"`**）包裹住这个参数**，例如：

```cpp
#define STR(x) #x
```

调用 `STR(something)` 就会被替换为 `"something"`。

## 参考链接：

- [https://gcc.gnu.org/onlinedocs/gcc-4.3.6/cpp/Stringification.html](https://gcc.gnu.org/onlinedocs/gcc-4.3.6/cpp/Stringification.html)

# 二、连接参数：`##` 操作符

`##` 操作符的作用是将两个宏参数**连接**起来，例如：

```cpp
#define CONCAT(a, b) a##b
```

调用 `CONCAT(gcc, -11)` 就会被替换为 `gcc-11`。

## 参考链接：

- [https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html](https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html)

# 三、注意事项

这两个操作符不会先展开其参数，而是直接使用传进来的实参，即使这个实参包含嵌套的宏。在 GCC 文档中关于 [Argument Prescan](https://gcc.gnu.org/onlinedocs/gcc-4.3.6/cpp/Argument-Prescan.html#Argument-Prescan) 有这样的描述：

> Macro arguments are completely macro-expanded before they are substituted into a macro body, **unless they are stringified or pasted with other tokens.**
> 

具体可以参考我的另一篇文章：[GCC 预处理：如何正确地将宏展开的结果转成字符串](/2022/02/21/cpp-stringify-macro-with-expansion.html)。