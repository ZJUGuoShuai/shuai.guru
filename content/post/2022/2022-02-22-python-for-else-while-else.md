---
date: "2022-02-22T20:01:24Z"
tags: ["Python"]
title: Python 中 for-else 和 while-else 语句的用处
toc: true
---

许多人可能不知道，Python 的 `for` 循环和 `while` 循环都支持一个可选的 `else` 从句，例如：

```python
for i in range(100):
    # for 循环内部
else:
    # else 从句
```

它是干什么的呢？

# else 从句什么时候会被执行？

我们都知道，在 `if-else` 语句中，只有 `if` 条件不满足，`else` 才会被执行。但在 `for-else` 这里并没有出现条件呀？

其实，`else` 部分能否执行，主要看 `for` 循环部分，它**能否正常执行完整个循环**。这里的正常执行完，具体来说就是：

1. 不能通过 `break` 跳出循环；
2. 不能通过 `return` 从当前函数返回（如果有的话）；
3. 不能通过 `throw` 抛出异常，使得程序中止；

只要 `for` 循环能够正常结束，就会执行 `else` 部分。例如：

```python
for i in range(5):
    print(i)
else:
    print('Iterated over everything :)')
```

就会在输出：

```python
0
1
2
3
4
Iterated over everything :)
```

# 那这有什么用呢？

这个语法结构的作用主要是为了简化这种情况：

> 我需要遍历某个集合/列表等，寻找其中符合某个条件的元素，如果找到了，就跳出循环；如果不到，则需要执行应急代码。
> 

用 Python 代码来说，就是：

```python
for x in data:
    if meets_condition(x):
        break
else:
    # raise error or do additional processing
```

这种结构，如果用 C++ 来写，就比较麻烦，需要用某个 flag 变量来记录，是否找到了满足条件的元素，如：

```cpp
bool found = false;
for (auto x : list) {
    if (meets_condition(x)) {
        found = true;
        break;
    }
}

if (!found) {
    // raise error or do additional processing
}
```

可以很明显地看到，Python 的 `else` 从句使得整个过程写起来非常简洁、优雅并且 Pythonic。这就是 `for-else` 语句或 `while-else` 语句的主要功能。

# 参考链接

- [The Forgotten Optional `else` in Python Loops](https://s16h.medium.com/the-forgotten-optional-else-in-python-loops-90d9c465c830)