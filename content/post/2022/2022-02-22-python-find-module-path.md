---
date: "2022-02-22T00:00:00Z"
tags: ["Python"]
title: 快速找到 Python 模块所在文件的路径
---

今天发现一个很好用的技巧，可以快速找到某个 Python 模块所在文件的路径，也可以用来快速分辨某个模块是纯 Python 实现的还是 C/C++ 实现的扩展模块。

# 方法

只要打印这个模块的 `__file__` 属性即可。例如：

```python
import math
import os

print(math.__file__)
print(os.__file__)
```

输出结果：

```python
/home/guo/miniconda3/lib/python3.9/lib-dynload/math.cpython-39-x86_64-linux-gnu.so
/home/guo/miniconda3/lib/python3.9/os.py
```

从这里可以看出，Python 标准库中的 `math` 模块其实是一个 **C 扩展模块**，因为它是 `.so` 文件；而 `os` 模块则是普通模块，它是纯 Python 文件。