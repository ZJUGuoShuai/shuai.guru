#define MAX_NUM 100000000

float variance_two_pass(float* x, int N) {
  // one pass for mean
  float mean = 0.0f;
  for (int i = 0; i < N; i++)
    mean += x[i];
  mean /= N;

  // one pass for var
  float sum_of_squares = 0.0f;
  for (int i = 0; i < N; i++) {
    float d = x[i] - mean;
    sum_of_squares += d * d;
  }

  return sum_of_squares / N;
}

float variance_one_pass(float* x, int N) {
  float mean = 0.0f;
  float sum_of_squares = 0.0f;

  for (int i = 0; i < N; i++) {
    mean += x[i];
    sum_of_squares += x[i] * x[i];
  }
  mean /= N;

  return sum_of_squares / N - mean * mean;
}

float variance_Welford(const float* x, int N) {
  float mean = 0.0f;
  float s = 0.0f;

  for (int i = 0; i < N; i++) {
    float x_i = x[i];
    float d1 = x_i - mean;
    mean += d1 / (i + 1);
    float d2 = x_i - mean;
    s += d1 * d2;
  }

  return s / N;
}

static void BenchmarkVarTwoPass(benchmark::State& state) {
  float* x = new float[MAX_NUM];
  float y;
  for (auto _ : state) {
    benchmark::DoNotOptimize(x);
    benchmark::DoNotOptimize(y);
    y = variance_two_pass(x, MAX_NUM);
  }
  delete[] x;
}

static void BenchmarkVarOnePass(benchmark::State& state) {
  float* x = new float[MAX_NUM];
  float y;
  for (auto _ : state) {
    benchmark::DoNotOptimize(x);
    benchmark::DoNotOptimize(y);
    y = variance_one_pass(x, MAX_NUM);
  }
  delete[] x;
}

static void BenchmarkVarWelford(benchmark::State& state) {
  float* x = new float[MAX_NUM];
  float y;
  for (auto _ : state) {
    benchmark::DoNotOptimize(x);
    benchmark::DoNotOptimize(y);
    y = variance_Welford(x, MAX_NUM);
  }
  delete[] x;
}

BENCHMARK(BenchmarkVarTwoPass);
BENCHMARK(BenchmarkVarOnePass);
BENCHMARK(BenchmarkVarWelford);
