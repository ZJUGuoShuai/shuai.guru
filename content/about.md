---
layout: page
title: 关于我
permalink: /about/
---

**SHUAI.GURU**

Linux、C++、Docker、CUDA、Python；字体强迫症，代码洁癖。

前浙江大学[求是潮](https://www.qsc.zju.edu.cn/)／[技术研发中心](https://github.com/QSCTech)成员。

最喜欢的**音乐艺人**有 Kanye West、Kendrick Lamar、Drake、J. Cole、Travis Scott、Lil Wayne、Dr. Dre、Eminem、Jay-Z 等。

最喜欢的**非技术类书籍**有《黑客与画家》、《未来简史》、《人类简史》、《三体》系列、《一个人的朝圣》、《2001：太空漫游》、《动物农场》等。

最喜欢的**电影**：《星际穿越》、《蝙蝠侠黑暗骑士》三部曲、《银翼杀手 2049》。

# 友情链接

- [Bluegill](https://hxhue.github.io/)
- [PINK_PEACH_STAR](https://blog.csdn.net/PINK_PEACH_STAR)
- [Sun's Blog](https://www.virgilsun.com/)
- [Shall We Code?](https://waynerv.com/)

# 关于本站

本站使用 [Hugo](https://gohugo.io/) 构建，托管于 [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html)。

- GitHub：[AtomicVar](https://github.com/AtomicVar)
- Stack Overflow：[Shuai](https://stackoverflow.com/users/8102500/shuai)

<script src="https://giscus.app/client.js"
        data-repo="ZJUGuoShuai/blog-comments"
        data-repo-id="R_kgDOH0RpnQ"
        data-category="Announcements"
        data-category-id="DIC_kwDOH0Rpnc4CQ0Qm"
        data-mapping="title"
        data-strict="0"
        data-reactions-enabled="1"
        data-emit-metadata="0"
        data-input-position="top"
        data-theme="light"
        data-lang="zh-CN"
        data-loading="lazy"
        crossorigin="anonymous"
        async>
</script>